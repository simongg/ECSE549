

    ;define templates
  
;the templates list "objects",and facts about them
  
   	(deftemplate robot 
   	(slot driving)
    (slot walking)
    (slot status)
    )
 
   	(deftemplate light 
   	(slot r)
    (slot g)
    (slot b-r)
    (slot b-g)
    (slot b-y)
    (slot none)
    )
   
	(deftemplate w-s
    (slot walk) 
    (slot dont-walk)
    )
  ;define rules - the rules are in several blocks
;
;
;Rule for not driving - any red light

    (defrule red-light
		(and (light (r TRUE)) (robot (status "d")))
	=>
    ;
    ; ask if the light is red
    ;
    	(assert (robot (driving FALSE)))
			(printout t "Robot is not driving" crlf)
    )
	
	(defrule green-light 
		(and (light (g TRUE)) (robot (status "d")))
	=>
    	(assert (robot (driving TRUE)))
			(printout t "Robot is driving" crlf)
    )
	
	(defrule blinking-red-light 
		(and (light (b-r TRUE)) (robot (status "d")))
	=>
    	(assert (robot (driving FALSE)))
			(printout t "Robot is not driving" crlf)
    )

	(defrule blinking-green-light
		(and (light (b-r TRUE)) (robot (status "d")))	
    =>
    	(assert (robot (driving TRUE)))
			(printout t "Robot is turning" crlf)
    )

	(defrule blinking-yellow-light ""
		(and (light (b-y TRUE)) (robot (status "d")))
	=>
    	(assert (robot (driving TRUE)))
			(printout t "Robot is driving" crlf)
    )

	(defrule none-light ""
		(and (light (none TRUE)) (robot (status "d")))
	=>
    	(assert (robot (driving FALSE)))
			(printout t "Robot is not driving" crlf)
    )

    (defrule check-walk "" 
    	(and (w-s ( walk TRUE)) (robot (status "w")))
 	=>
  		(assert (robot (walking TRUE) ))
			(printout t "the robot is not walking" crlf)
    ) 

	(defrule check-dont-walk "" 
    	(and (w-s ( dont-walk TRUE)) (robot (status "w")))
 	=>
  		(assert (robot (walking FALSE) ))
			(printout t "the robot is not walking" crlf)
    ) 

(reset)
(assert (light (r TRUE)))
(assert (robot (status "d")))
(assert (w-s (dont-walk TRUE)))
(facts)
(run)
