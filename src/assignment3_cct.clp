;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Half adder circuit using rules in JESS
;
; Simon Geoffroy-Gagnon
; 260856157
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; define all the connections in the adder cct

(defrule 1F1-1X1-1A1 ; input 1 of F1 to X1 and A1
    (IN 1 F 1 ?v)
 =>
    (assert (IN 1 X 1 ?v))
    (assert (IN 1 A 1 ?v))
)

(defrule 2F1-2X1-2A1 ; second input of F1 to second input of X1 and A1
    (IN 2 F 1 ?v)
 =>
    (assert (IN 2 X 1 ?v))
    (assert (IN 2 A 1 ?v))
)

(defrule 3F1-2X2-1A2 ; input 3 to input 2 of X1 and input 1 of A2
    (IN 3 F 1 ?v)
=>
	(assert (IN 2 X 2 ?v))
    (assert (IN 1 A 2 ?v))
)

(defrule X1-1X2-2A2 ; connect output of X1 to input 1 of X2 and input 2 of A2 
    (OUT X 1 ?v) 
=> 
    (assert (IN 1 X 2 ?v)) 
    (assert (IN 2 A 2 ?v))
)

(defrule A2-1O1 ; output of A2 into input 1 of O1
    (OUT A 2 ?v)
=>
    (assert (IN 1 O 1 ?v))
)

(defrule A1-2O1 ; output of A1 into input 2 of O1
    (OUT A 1 ?v)
=>
    (assert (IN 2 O 1 ?v))
)

(defrule X2-1F1 ;OUTPUT OF X1 TO OUTPUT 1 OF F1 (sum bit)
    (OUT X 2 ?v)
=>
    (assert (OUT 1 F 1 ?v))
)

(defrule O1-2F1 ;OUTPUT OF O1 TO OUTPUT 2 OF F1 (carry bit)
    (OUT O 1 ?v)
=>
    (assert (OUT 2 F 1 ?v))
)


;define all rules for each gate: X - xor, O - or, A - and

(defrule X ; xor gate gives a 1 if an odd number of inputs are 1s
    (IN 1 X ?gateNumber ?v1)
    (IN 2 X ?gateNumber ?v2)
=>
   (if (or (and (= ?v1 1) (= ?v2 0)) 
            (and (= ?v1 0) (= ?v2 1))
       	)
   then 
    	(assert (OUT X ?gateNumber 1))
   else
        (assert (OUT X ?gateNumber 0))
    )
)


(defrule O ; or gate gives 1 unless all inputs are 0
    (IN 1 O ?gateNumber ?v1)
    (IN 2 O ?gateNumber ?v2)
=>
    (if (or (= ?v1 1) (= ?v2 1)) 
     then
        (assert (OUT O ?gateNumber 1))
    else
        (assert (OUT O ?gateNumber 0))
    )
)

(defrule A ; and gate gives 0 unless all inputs are 1
    (IN 1 A ?gateNumber ?v1)
    (IN 2 A ?gateNumber ?v2)
=>
    (if (or (= ?v1 0) (= ?v2 0)) 
     then
        (assert (OUT A ?gateNumber 0))
    else
        (assert (OUT A ?gateNumber 1))
    )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;once we have both outputs, print
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule output
    (OUT 1 F 1 ?v1)
    (OUT 2 F 1 ?v2)
=>
    (printout t crlf "The half adder yields a bit value of " ?v1 
        " for the sum and bit value of " ?v2 " for the overflow" crlf)
)

(defrule start 
    (initial-fact)
=>
    (printout t crlf "Value for input bit 1? (1, 0):")
    (assert (IN 1 F 1 =(read)))
    (printout t crlf "Value for input bit 2? (1, 0):")
    (assert (IN 2 F 1 =(read)))
    (printout t crlf "Value for input bit 3 (the carry bit)? (1, 0):")
    (assert (IN 3 F 1 =(read)))
)

(reset)
(run)
;(facts)
;(rules)
