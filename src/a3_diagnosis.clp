;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
; Part 2 - Diagnosis
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule diagnose
	(IN 1 F 1 ?i1) 
    (IN 2 F 1 ?i2)
    (IN 3 F 1 ?i3) 
    (OUT 1 F 1 ?sum) 
    (OUT 2 F 1 ?carry)
=>
    ;(printout t crlf ?i1 ?i2 ?i3 ?sum ?carry crlf)
    ; if theres a problem with the sum bit output, the problem is starting in x2
    (if (or (and (<> ?i1 ?i2) (= ?i3 1) (= ?sum 1)) ; if i1 != i2, and i3 == 1
            (and (<> ?i1 ?i2) (= ?i3 0) (= ?sum 0)) 
            (and (= ?i1 ?i2) (= ?i3 1) (= ?sum 0))
            (and (= ?i1 ?i2) (= ?i3 0) (= ?sum 1))
        )
        then
    (assert (problem_x2)))

    ; if theres a problem with the carry bit output, problem starts in o1
    (if (or (and (= ?i1 ?i2 1) (= ?carry 0))
            (and (= ?i1 ?i2 0) (= ?carry 1))
            (and (<> ?i1 ?i2) (neq ?carry ?i3))
            )
         then       
(assert (problem_o1))))

(defrule print_problem_o1_x2
    (problem_o1)
    (problem_x2)
=>
    (printout t crlf "There is a problem with both the sum bit and the carry bit, which means anything could be broken in the adder circuit:")
)   
 
(defrule print_problem_o1
    (problem_o1)
=>
    (printout t crlf "A problem starting in O1 could be caused by either A1, A2, or O1" crlf)
    ;(printout t crlf "If sum bit 1 and sum bit 2 are both set to 1, and input carry bit set to 0, what is the output carry bit?" crlf)
)

(defrule print_problem_x2
    (problem_x2)
=>
    (printout t crlf "A problem starting in X2 could be caused by either X1 or X2." crlf)
)

(defrule start 
    (initial-fact)
=>
    (printout t crlf "Value for input bit 1? (1, 0):")
    (assert (IN 1 F 1 =(read)))
    (printout t crlf "Value for input bit 2? (1, 0):")
    (assert (IN 2 F 1 =(read)))
    (printout t crlf "Value for input bit 3 (the carry bit)? (1, 0):")
    (assert (IN 3 F 1 =(read)))
    (printout t crlf "Value for output bit 1, the sum bit? (1, 0):")
    (assert (OUT 1 F 1 =(read)))
    (printout t crlf "Value for output bit 2, the carry bit? (1, 0):")
    (assert (OUT 2 F 1 =(read)))
)

(reset)
(run)
(facts)
