; Setup bars template
	(deftemplate bars
	(slot name (type STRING)) 
    (slot len (type INTEGER))
	(slot wid (type INTEGER))
	(slot hei (type INTEGER))
    (slot scs)
    (slot side)
    (slot cube)
    (slot printed (default FALSE))
	)

; Set up what were searching for
	(deftemplate search_template
    (slot scs)
    (slot side)
    (slot cube)
    )

; Set up all of the different bars
	(deffacts bar
	(bars (name "te1")(len 10) (wid 10) (hei 10))
	(bars (name "cu2")(len 20) (wid 10) (hei 10))
	(bars (name "cu3")(len 20) (wid 10) (hei 20))
	(bars (name "pt4")(len 10) (wid 20) (hei 10))
	(bars (name "fe5")(len 30) (wid 30) (hei 10))
	(bars (name "fe6")(len 30) (wid 20) (hei 10))
	(bars (name "fe7")(len 10) (wid 30) (hei 20))
	(bars (name "cu8")(len 10) (wid 20) (hei 30))
	(bars (name "pt9")(len 30) (wid 20) (hei 10))
	(bars (name "pt10")(len 20) (wid 20) (hei 20))
	)

; Check if square cross section
(defrule scs
    ?b <- (bars (len ?len) (wid ?wid) (hei ?hei)) 
    (test (= ?len ?wid))
    =>
    (modify  ?b (scs TRUE))
)

; Check if side
(defrule side
    ?b <- (bars (len ?len) (wid ?wid) (hei ?hei)) 
    (test (= ?len ?hei))
    =>
    (modify  ?b (side TRUE))
)

; Check if cube
(defrule cube
    ?b <- (bars (len ?len) (wid ?wid) (hei ?hei)) 
    (test (= ?len ?wid ?hei))
    =>
    (modify  ?b (cube TRUE))
)

(defrule find_cubes
    ?b <- (bars)
    (and (bars (cube TRUE) (name ?name) (printed FALSE))(search_template (cube TRUE)))
    =>
    (modify ?b (printed TRUE))
    (printout t ?name crlf)
)

(defrule find_scs
    ?b <- (bars)
    (and (bars (scs TRUE) (name ?name) (printed FALSE))(search_template (scs TRUE)))
    =>
    (modify ?b (printed TRUE))
    (printout t ?name crlf)
)

(defrule find_sides
    ?b <- (bars)
    (and (bars (side TRUE) (name ?name) (printed FALSE))(search_template (side TRUE)))
    =>
    (modify ?b (printed TRUE))
    (printout t ?name crlf)
)

(reset)
(assert (search_template (scs TRUE)))
(run)
(facts)