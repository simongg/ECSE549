; Assignemnt 1 Question 3
; Simon Geoffroy-Gagnon, 260856157
; Got this template from Chris Morin's Github

; Questions are triggered by the existance of question facts
; After all answers are received, the program prints out possible
; car problems (this could be improved)

(deftemplate car
    (slot gas)
    (slot gas_printed (default FALSE))
    (slot electrical)
    (slot electrical_printed (default FALSE))
    (slot battery)
    (slot battery_printed (default FALSE))
    (slot starter)
    (slot starter_printed (default FALSE))
    (slot oil)
    (slot oil_printed (default FALSE))
    (slot tires)
    (slot tires_printed (default FALSE))
)
(deftemplate question
	(slot text)
	(slot problem)
    (slot solution)
 )
(deffacts diagnostic-questions
    (question (text "Does the engine start up when the ignition key is turned?") (problem starter)
         (solution "Please fix the starter motor"))
	(question (text "Does the fuel gauge read full?") (problem gas) (solution "Please fill up the gas tank"))
	(question (text "Do the car lights work?") (problem battery) (solution "Please replace the battery"))
   	(question (text "Is the tire pressure between 28 ls and 32 lbs?") (problem tyres) (solution "Please pump up the tires"))
	(question (text "Does the oil dipstick indicate full?") (problem oil) (solution "Please add some oil"))
)
(deffacts diagnose_me
    (car)
)
(defrule diagnose
    (question (text ?t) (problem ?p) (solution ?s))
=>
	(printout t ?t " ")
	(printout t "(y/n) ")
	(bind ?answer (read))
	(if (eq ?answer end) then (clear)) ; this if statement is, sadly, required in this case
	(if (eq ?answer n) then (assert (problem ?p)) (printout t ?s crlf crlf) ) ; if the answer is no, then 
    ; assert the problem and print out a solution. After we run this section and finish asking all of the 
    ; questions, we will print out a full list of issues encountered.
)

(reset)

(run)
(defrule gas_issue
    ?c <- (car)
	(problem gas)
    (car (gas_printed FALSE))
=>
	(printout t "Gas tank is empty" crlf)
    (modify ?c (gas FALSE) (gas_printed TRUE))
)
(defrule electrical_issue
    (or (problem battery) (problem starter))
    ?c <- (car)
    (car (electrical_printed FALSE))
    =>
    (modify ?c (electrical FALSE) (electrical_printed TRUE))
    (printout t "the electrical system is faulty" crlf)
    )
(defrule battery_issue
	(problem battery)
    ?c <- (car)
    (car (battery_printed FALSE))
    =>
    (modify ?c (battery FALSE) (battery_printed TRUE))
	(printout t "The battery is faulty" crlf)
)
(defrule starter_issue
	(problem starter)
    ?c <- (car)
    (car (starter_printed FALSE))
    =>
    (modify ?c (starter FALSE) (starter_printed TRUE))
	(printout t "The starter motor might be broken." crlf)
)
(defrule oil_issue
	(problem oil)
    ?c <- (car)
    (car (oil_printed FALSE))
    =>
    (modify ?c (oil FALSE) (oil_printed TRUE))
	(printout t "The oil is low" crlf)
)
(defrule tire_issue
	(problem tyres)
    ?c <- (car)
    (car (tires_printed FALSE))
    =>
    (modify ?c (tires FALSE) (tires_printed TRUE))
	(printout t "Tires pressure is low" crlf)
) 
(run)
(facts)