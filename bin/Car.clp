

    ;define templates
  
;the templates list "objects",and facts about them
  
   (deftemplate person 
   (slot key )
   (slot cash )
   (slot late))
 
   (deftemplate car 
   (slot gas )
   (slot running ))
    
  ;define rules - the rules are in several blocks
;
;the first set checks on the running status of the car - these two rules need inputs, i.e. the person having a 
;key and the car having gas
;
;Rule for not running - there should be a second rule if the person does not have a key

    (defrule check-if-not-running "" 
    (or (car (gas FALSE) )  (person (key FALSE)))
 	=>
  	(assert (car (running FALSE) ))
		(printout t "the car is not running" crlf)) 

;
;Rule for running - only running if person has key and there is gas in the car
;
	(defrule check-if-running "" 
  	(car (gas TRUE) )
    (person (key TRUE))
  	=>
  	(assert (car (running TRUE) )))
;
;The rules above have determined the state of the car, if it was unknown
;next,if the car is not running because of no gas, you need to buy some - so the person willl need cash - check this
;

	(defrule have-cash ""
	(car (running FALSE))
	(person (key TRUE))
	=>
    ;
    ; ask the question about cash because the car is not running - we could have used the gas fact instead here.
    ;
	(printout t "Do you have cash? (yes/no)" crlf)
    ;
    ;expect input to assert the fact related to cash in the person template
    ;
    (assert (person (cash (read))) ))
;
;finally, determine if the person will be late (set the fact in the person template) given the facts known
; and then print out the result
;
	(defrule not-late-state 
    (person (cash yes)) 
    =>  
    (assert (person (late FALSE))))

	(defrule late-state
    (person (cash no))
    =>
    (assert (person (late TRUE))))
;
; now output the conclusion - late or not?
;
	(defrule going-to-be-late 
    (person (late TRUE))
    =>
    (printout t "you will be late" crlf))

	(defrule on-time 
    (or(person (late FALSE))(car (running TRUE)))
    =>
    (printout t "you will make your meeting" crlf))

; run the system - reset all the fact, assert initial information (facts) and run
;
(reset)
(assert (car (gas FALSE)))
(assert (person (key TRUE)))
(facts)
(run)
